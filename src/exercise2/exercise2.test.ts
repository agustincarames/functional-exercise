import { myMap } from "./exercise2";

describe('myMap', () => {
  it('Empty list', () => {
    expect(myMap([], () => null)).toStrictEqual([]);
  });

  it('One element list', () => {
    expect(myMap([1], n => 2 * n)).toStrictEqual([2]);
  });

  it('Two element list', () => {
    expect(myMap([1, 2], n => n >= 2)).toStrictEqual([false, true]);
  });
});