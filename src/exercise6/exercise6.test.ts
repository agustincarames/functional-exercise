import { Action, determineWhenToRun } from './exercise6';

describe('determineWhenToRun', function() {
  it('Will never run if there are no actions', () => {
    expect(determineWhenToRun([])).toStrictEqual(null);
  });

  it('Will run a single action on expiry', () => {
    const action: Action = {
      mustRunBeforeDelay: 100,
      canRunAfterDelay: 50,
      command: 'do something',
    };
    expect(determineWhenToRun([action])).toStrictEqual({
      delay: 100,
      actions: [action],
    });
  });

  it('Will not run all actions', () => {
    const trigger: Action = {
      mustRunBeforeDelay: 100,
      canRunAfterDelay: 0,
      command: 'do something',
    };
    const willRun: Action = {
      mustRunBeforeDelay: 200,
      canRunAfterDelay: 10,
      command: 'do something',
    };
    const willNotRun: Action = {
      mustRunBeforeDelay: 300,
      canRunAfterDelay: 105,
      command: 'do something',
    };

    expect(determineWhenToRun([trigger, willRun, willNotRun])).toStrictEqual({
      delay: 100,
      actions: [trigger, willRun],
    });
  });
});
