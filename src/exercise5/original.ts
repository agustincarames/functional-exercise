import { Category } from './types';

const getPriceAux = (category: Category): number => {
  switch (category) {
    case Category.One:
      return 123;
    case Category.Two:
      return getPriceAux(Category.Five) - getPriceAux(Category.One) - 210;
    case Category.Five:
      return 789;
  }
};

export const getPrice = (category: Category, offer: boolean): number => {
  const discount = offer ? getPrice(Category.Two, false) / 8 : 0;

  let value = getPriceAux(Category.Five) * 0.001267427;
  switch (category) {
    case Category.One:
      if (offer) {
        value += getPrice(category, false) - 1;
      } else {
        value *= getPriceAux(category);
        value *= 0.813;
      }
      break;
    case Category.Two:
      return 2 * getPrice(Category.One, offer) + discount;
    case Category.Five:
      return getPriceAux(Category.Two) + 44 - discount;
  }
  return Math.round(value) - discount;
};
