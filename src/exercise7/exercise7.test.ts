import { derive } from './exercise7';

const square = (x: number) => x * x;

describe('derive', function() {
  it('Approximate the derivative of square in point 2 with delta 0.001.', () => {
    const derivefn = derive(square, 0.001);
    expect(derivefn(2)).toBeCloseTo(4);
  });

  it('Approximate the derivative of square in point 6 with delta 0.001.', () => {
    const derivefn = derive(square, 0.001);
    expect(derivefn(6)).toBeCloseTo(12);
  });

  it('Approximate the derivative of sine in point 0 with delta 0.001.', () => {
    const derivefn = derive(Math.sin, 0.001);
    expect(derivefn(0)).toBeCloseTo(Math.cos(0));
  });

  it('Approximate the derivative of cosine in point 0 with delta 0.001.', () => {
    const derivefn = derive(Math.cos, 0.001);
    expect(derivefn(0)).toBeCloseTo(-Math.sin(0));
  });
});
