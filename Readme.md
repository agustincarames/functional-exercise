This project contains exercises for Técnicas de Diseño - UBA

1. Install Node JS
   1. https://nodejs.org/en/
2. Fork repository, add ayudantesTecnicas as member
3. Run npm install
4. Run npm test (to run the actual tests)

Exercises:
1. Implement a function to calculate the Nth fibonacci number
2. Implement map without using map itself
3. Implement reduce without using reduce itself
4. Implement a function to calculate the average of an array
5. Refactor the function given in original.ts
6. Implement an extremely simplified task scheduling system that tries to group tasks
7. Implement a higher-order function that receives a numerical function and returns its derivative function, estimated by central difference, that is:

   f'(x) ~ (f(x + h) - f(x - h)) / (2 * h)

8. Implement some functions that reduce trees
9. Implement a simplified expression evaluator